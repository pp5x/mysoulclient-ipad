//
//  main.m
//  MySoul
//
//  Created by Pierre Pagnoux on 02/11/13.
//  Copyright (c) 2013 Pierre Pagnoux. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
