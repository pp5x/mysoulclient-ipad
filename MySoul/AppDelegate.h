//
//  AppDelegate.h
//  MySoul
//
//  Created by Pierre Pagnoux on 02/11/13.
//  Copyright (c) 2013 Pierre Pagnoux. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
