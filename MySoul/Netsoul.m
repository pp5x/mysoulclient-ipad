//
//  Netsoul.m
//  MySoul
//
//  Created by Pierre Pagnoux on 02/11/13.
//  Copyright (c) 2013 Pierre Pagnoux. All rights reserved.
//

#import "Netsoul.h"
#import <sys/socket.h>
#import <arpa/inet.h>
#import <CommonCrypto/CommonDigest.h>

/*
** Netsoul IP : external 163.5.255.5
** Netsoul IP : internal 10.42.1.59
*/
#define HOST "163.5.255.5"
#define PORT 4242
#define BUFFER_SIZE 500

#define AUTH_AG "auth_ag ext_user none none\n"
#define LOCATION "iPad"
#define USERAGENT "MySoulv0.1b"

@interface Netsoul()

@property int socket;
@property char *buffer;

@property char *hash;
@property char *ip;
@property char *port;
@property char *timestamp;

@property char *auth;

@end

@implementation Netsoul
-(id)init {
    self = [super init];

    if (self) {
        self.buffer = malloc(BUFFER_SIZE + 1);
    }

    return self;
}

-(BOOL)connect {
    self.socket = socket(PF_INET, SOCK_STREAM, 6);
    if (self.socket < 0)
      {
        NSLog(@"[CONNECT] Failed to get socket!");
        return FALSE;
      }

    struct sockaddr_in server;

    memset(&server, 0, sizeof (struct sockaddr_in));
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr(HOST);
    server.sin_port = htons(PORT);

    if (connect(self.socket,
                (struct sockaddr*)&server,
                sizeof (struct sockaddr)))
      {
        NSLog(@"[CONNECT] Error while connecting to the server. %s.",
              strerror(errno));
        return FALSE;
      }

    ssize_t length = 0;

    length = recv(self.socket, self.buffer, BUFFER_SIZE, 0);
    self.buffer[length] = '\0';

    char *ptr = self.buffer;
    if (!strcmp(strsep(&ptr, " "), "salut"))
        NSLog(@"[SERVER] 'salut' matched");
    else
      {
        NSLog(@"[SERVER] 'salut' not matched. Fail.");
        return FALSE;
      }

    NSLog(@"[SERVER] Socket: %s", strsep(&ptr, " "));
    self.hash = strdup(strsep(&ptr, " "));
    self.ip = strdup(strsep(&ptr, " "));
    self.port = strdup(strsep(&ptr, " "));
    self.timestamp = strdup(strsep(&ptr, " "));

    NSLog(@"[SERVER] Hash: %s", self.hash);
    NSLog(@"[SERVER] IP: %s", self.ip);
    NSLog(@"[SERVER] Port: %s", self.port);
    NSLog(@"[SERVER] Timestamp: %s", self.timestamp);

    [self genAuth];
    return TRUE;
}

-(void)genAuth {
    sprintf(self.buffer,
            "%s-%s/%s%s",
            self.hash,
            self.ip,
            self.port,
            [self.pass UTF8String]);
    unsigned char digest[16];

    CC_MD5(self.buffer, strlen(self.buffer), digest);

    self.auth = malloc(sizeof (char) * (CC_MD5_DIGEST_LENGTH * 2 + 1));
    memset(self.auth, 0, (CC_MD5_DIGEST_LENGTH * 2 + 1));

    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        sprintf(self.auth + (2 * i), "%02x", digest[i]);
    }

    NSLog(@"[AUTH] : %s", self.auth);
}

-(int)expect {
    NSLog(@"[Expect] %s", self.buffer);
    return strncmp(self.buffer, "rep 002 --", 10);
}

-(BOOL)signIn {
    NSLog(@"[SignIn] Login in ...");
    send(self.socket, AUTH_AG, sizeof (AUTH_AG) - 1, 0);

    ssize_t length = 0;
    length = recv(self.socket, self.buffer, BUFFER_SIZE, 0);
    self.buffer[length] = '\0';

    if ([self expect])
        return FALSE;

    length = sprintf(self.buffer,
                     "ext_user_log %s %s %s %s\n",
                     [self.login UTF8String],
                     self.auth,
                     LOCATION,
                     USERAGENT);

    send(self.socket, self.buffer, length, 0);
    length = recv(self.socket, self.buffer, BUFFER_SIZE, 0);
    self.buffer[length] = '\0';

    if ([self expect])
        return FALSE;

    NSLog(@"[SignIn] Succeed !");

    return TRUE;
}

-(void)dealloc {
    free(self.buffer);
    free(self.hash);
    free(self.ip);
    free(self.port);
    free(self.auth);
}

-(void)disconnect {
    if (shutdown(self.socket, SHUT_RDWR) < 0)
        NSLog(@"[DISCONNECT] %s", strerror(errno));
    free(self.hash);
    free(self.ip);
    free(self.port);
    free(self.auth);
}
@end
