//
//  ViewController.m
//  MySoul
//
//  Created by Pierre Pagnoux on 02/11/13.
//  Copyright (c) 2013 Pierre Pagnoux. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property Netsoul *client;
@end

@implementation ViewController

-(id)init {
    self = [super init];
    self.client = [[Netsoul alloc] init];

    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.client = [[Netsoul alloc] init];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)setLogin:(UITextField *)sender {
    self.client.login = sender.text;
    NSLog(@"Login has changed : %@", self.client.login);
}

- (IBAction)setPassword:(UITextField *)sender {
    self.client.pass = sender.text;
    NSLog(@"Password has changed : %@", self.client.pass);
}

- (IBAction)activation:(UISwitch *)sender {
    NSLog(@"Connection switch : %@", sender.isOn ? @"YES" : @"NO");

    if (sender.isOn)
      {
        if (!([self.client connect] &&
              [self.client signIn]))
          {
            [sender setOn:FALSE animated:YES];
            [self.client disconnect];
          }
      }
    else
        [self.client disconnect];
}

- (IBAction)about:(id)sender {
    [loginView setHidden:![loginView isHidden]];
    [credits setHidden:![credits isHidden]];
}
@end
