//
//  Netsoul.h
//  MySoul
//
//  Created by Pierre Pagnoux on 02/11/13.
//  Copyright (c) 2013 Pierre Pagnoux. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Netsoul : NSObject

@property NSString *login;
@property NSString *pass;

- (BOOL)connect;
- (void)disconnect;
- (BOOL)signIn;

@end
