//
//  ViewController.h
//  MySoul
//
//  Created by Pierre Pagnoux on 02/11/13.
//  Copyright (c) 2013 Pierre Pagnoux. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Netsoul.h"

@interface ViewController : UIViewController
{
    IBOutlet UILabel *credits;
    IBOutlet UIView *loginView;
    IBOutlet UIBarButtonItem *about_button;
}

- (IBAction)about:(id)sender;
@end
